const button = document.querySelector("#button")
const MIC_ON = true
const MIC_OFF = false
const sounds = {false: document.querySelector("#audio-mic_off"),
                true: document.querySelector("#audio-mic_on")}

sounds[MIC_ON].load()
sounds[MIC_OFF].load()

function playSound(sound) {
  sounds[sound].currentTime = 0;
  sounds[sound].play()
}

function button_down(type) {
  if(button.dataset.pressed !== "false") return;
  console.log(`Down. (${type})`)
  button.dataset.pressed = true
  playSound(MIC_ON)
}

function button_up(type) {
  if(button.dataset.pressed !== "true") return;
  console.log(`And up. (${type})`)
  button.dataset.pressed = false
  playSound(MIC_OFF)
}

button.addEventListener('mousedown', ()=>button_down('mouse'))
window.addEventListener('mouseup', ()=>button_up('mouse'))

window.addEventListener('touchstart', ()=>button_down('touch'))
window.addEventListener('touchend', ()=>button_up('touch'))
